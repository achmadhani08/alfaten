<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartsController extends Controller
{
    public function __construct()
    {
        return $this->middleware(['auth', 'role:customer']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carts = Transaction::where('user_id', Auth::user()->id)
            ->with('product.discounts')
            ->groupBy('product_id')
            ->get();
        $cartTotal = Transaction::where('user_id', Auth::user()->id)->where('status', 'unpaid')->get();
        $jumlah = $cartTotal->sum('quantity');
        if ($jumlah > 99) {
            $jumlah = "99+";
        }

        $total_harga = 0;

        foreach ($carts as $cart) {
            $total_harga += $cart->product->price * $cart->quantity;
        }

        foreach ($carts as $product) {
            $product->product->discounts = Discount::where('product_id', $product->product->id)
                ->where('start_date', '<=', date('Y-m-d'))
                ->where('end_date', '>=', date('Y-m-d'))
                ->first();

            if ($product->product->discounts === null) {
                $product->product->discounts = (object) [
                    "percentage" => 0
                ];
                $product->product->new_price = $product->product->price;
            } else {
                $product->product->potongan = $product->product->discounts->percentage / 100 * $product->product->price;
                $product->product->new_price = $product->product->price - $product->product->potongan;
            }

            // dd($product->product);
        }


        $carts = Transaction::where('user_id', Auth::user()->id)->where('status', 'unpaid')->get();
        $jumlah = $carts->sum('quantity');
        // dd($cart->product);
        // dd($total_harga);

        return view('customer.cart',  compact('carts', 'total_harga', 'jumlah'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Transaction::find($id)->delete();

        return redirect()->back();
    }

    public function editQty(Request $request)
    {

        $check = Transaction::where("product_id", $request->product_id)->where("user_id", Auth::user()->id)->where("status", "unpaid")->with("product.discountS")->first();

        if ($check !== null) {
            Transaction::find($check->id)->update([
                "quantity" => $request->quantity
            ]);
        }

        return redirect()->back()->with('success', 'Berhasil Menambahkan produk kedalam Carts');
    }
}
